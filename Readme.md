About this repo
===============
This repo is intended to contain processed copies (not a clone) of the MakeHuman official source (see https://bitbucket.org/MakeHuman/makehuman).

History is stripped, assets are downloaded and processed, and superfluous files (such as development tools) are removed. 

Branches
--------
This is the 1.0.2 release branch. It is currently to be considered "trunk" for stable releases.

Building packages
-----------------
If you want to build a release for any platform, you have two directories to take into account:

* "makehuman" which contains the makehuman application
* "blendertools" which are optional scripts for blender integration

No compiling or transformation is usually necessary (since the application is pure python). 

Simply re-arrange the files into a directory hierarchy which fits you target platform. For example linux would want the makehuman directory to end up under /usr/share/makehuman.

