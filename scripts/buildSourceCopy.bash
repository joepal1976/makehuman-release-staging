#!/bin/bash

OLDCWD=`pwd`

STAGE_SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if [[ ! -n "$STAGE_HG_RELEASE_TAG" ]]; then
  STAGE_HG_RELEASE_TAG="1.0.2"
fi

if [[ ! -n "$STAGE_WORK_ROOT" ]]; then
  STAGE_WORK_ROOT="../.."
fi

if [[ ! -n "$STAGE_HG_CLONE_LOCATION" ]]; then
  STAGE_HG_CLONE_LOCATION="$STAGE_WORK_ROOT/makehuman-$STAGE_HG_RELEASE_TAG"
fi

if [[ ! -d $STAGE_HG_CLONE_LOCATION ]]; then
  hg clone https://bitbucket.org/MakeHuman/makehuman "$STAGE_HG_CLONE_LOCATION"
  cd "$STAGE_HG_CLONE_LOCATION"
  hg update $STAGE_HG_RELEASE_TAG
fi

cd $STAGE_SCRIPTDIR

rm -rf "$STAGE_WORK_ROOT/makehuman-processed-step-1" "$STAGE_WORK_ROOT/makehuman-processed-step-2"

cp -rfv "$STAGE_HG_CLONE_LOCATION/makehuman" "$STAGE_WORK_ROOT/makehuman-processed-step-1"

cd "$STAGE_WORK_ROOT/makehuman-processed-step-1"
find . -type d | sort > "../dirnames-before-processed.txt"
find . -type f | sort > "../filenames-before-processed.txt"

./download_assets.py
./compile_models.py
./compile_targets.py

find . -type d | sort > "../dirnames-after-processed.txt"
find . -type f | grep -v "\.pyc" | sort > "../filenames-after-processed.txt"

cd $STAGE_SCRIPTDIR

cp -rfv "$STAGE_WORK_ROOT/makehuman-processed-step-1" "$STAGE_WORK_ROOT/makehuman-processed-step-2"

cd "$STAGE_WORK_ROOT/makehuman-processed-step-2"

find . -name "*.target" -exec "rm" "-f" {} ";"
find . -name "*.obj" -exec "rm" "-f" {} ";"
find . -name "*.zip" -exec "rm" "-f" {} ";"
find . -name "*.pyc" -exec "rm" "-f" {} ";"

rm -rf testsuite

rm -f ./cleannpz.bat
rm -f ./cleannpz.sh
rm -f ./cleanpyc.bat
rm -f ./cleanpyc.sh
rm -f ./compile_models.py
rm -f ./compile_targets.py
rm -f ./download_assets.py
rm -f ./plugins/7_data.py
rm -f ./plugins/7_example.py
rm -f ./plugins/7_scene_editor.py
rm -f ./plugins/7_scripting.py
rm -f ./plugins/7_shell.py
rm -f ./plugins/7_targets.py
rm -f ./pylintrc

echo -n > data/VERSION "537:47eda58327ee"

patch < "$STAGE_SCRIPTDIR/patchmh.diff"

find . -type d | sort > "../dirnames-after-cleaned"
find . -type f | grep -v "\.pyc" | sort > "../filenames-after-cleaned"

cd $OLDCWD

