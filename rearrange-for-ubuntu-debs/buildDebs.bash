#!/bin/bash

OLDCWD=`pwd`

STAGE_SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if [[ ! -n "$STAGE_RELEASE_TAG" ]]; then
  STAGE_RELEASE_TAG="1.0.2"
fi

if [[ ! -n "$STAGE_WORK_ROOT" ]]; then
  STAGE_WORK_ROOT="../../deb"
fi

if [[ ! -n "$STAGE_SOURCE_LOCATION" ]]; then
  STAGE_SOURCE_LOCATION="$STAGE_SCRIPTDIR/../makehuman"
fi

if [[ ! -n "$STAGE_BLENDERTOOLS_LOCATION" ]]; then
  STAGE_BLENDERTOOLS_LOCATION="$STAGE_SCRIPTDIR/../blendertools"
fi

if [[ ! -d "$STAGE_WORK_ROOT" ]]; then
  mkdir -p "$STAGE_WORK_ROOT"
fi

DPKG="dpkg-deb -Z bzip2 -z 9 -b ../debroot"
SUBVER=0ubuntu1
DATE=`date -R`
TOTVER="${STAGE_RELEASE_TAG}-${SUBVER}"
DISTRO=trusty

declare -a debs=(makehuman-release makehuman-clothes makehuman-proxies makehuman-targets \
  makehuman-bodyparts makehuman-expressions makehuman-rigs makehuman-skins \
  makehuman-hair makehuman-data makehuman-i18n makehuman-plugins)

cd "$STAGE_SCRIPTDIR"

for f in "${debs[@]}"
do
  if [[ ! -e "control/$f" ]]; then
    touch "control/$f"
  fi
done

cd "$STAGE_WORK_ROOT"

rm -rf makehuman-src output

for f in "${debs[@]}"
do
  rm -rf $f
  mkdir -p $f
  mkdir -p $f/build
  mkdir -p $f/debroot
  mkdir -p $f/debroot/DEBIAN
  mkdir -p $f/debroot/usr/share/doc/$f
  cp "$STAGE_SCRIPTDIR/control/copyright" "$f/debroot/usr/share/doc/$f/"
  cat "$STAGE_SCRIPTDIR/control/changelog" | sed -e "s/VERSION/${TOTVER}/g"| sed -e "s/DISTRIBUTION/${DISTRO}/g" | gzip -9 > "$f/debroot/usr/share/doc/$f/changelog.Debian.gz"
done

cp -rfv "$STAGE_SOURCE_LOCATION" makehuman-src

chown -R 0:0 makehuman-src
chmod -R 644 makehuman-src
find makehuman-src -type d -exec "chmod" "755" {} ";"
find makehuman-src -name "*.py" -exec "chmod" "755" {} ";"
find makehuman-src -name "*.py" -exec "sed" "-i" "-e" 's/\/usr\/bin\/python$/\/usr\/bin\/python2.7/g' {} ";"

WORKROOT=`pwd`

### --- CLOTHES

cd "$STAGE_SCRIPTDIR"
cd "$STAGE_WORK_ROOT"

mkdir -p makehuman-clothes/debroot/usr/share/makehuman/data

cd $WORKROOT/makehuman-src/data

mv clothes $WORKROOT/makehuman-clothes/debroot/usr/share/makehuman/data/

### --- PROXIES

cd "$STAGE_SCRIPTDIR"
cd "$STAGE_WORK_ROOT"

mkdir -p makehuman-proxies/debroot/usr/share/makehuman/data

cd $WORKROOT/makehuman-src/data

mv proxymeshes $WORKROOT/makehuman-proxies/debroot/usr/share/makehuman/data/

### --- TARGETS

cd "$STAGE_SCRIPTDIR"
cd "$STAGE_WORK_ROOT"

mkdir -p makehuman-targets/debroot/usr/share/makehuman/data

cd $WORKROOT/makehuman-src/data

mv targets $WORKROOT/makehuman-targets/debroot/usr/share/makehuman/data/
mv targets.npz $WORKROOT/makehuman-targets/debroot/usr/share/makehuman/data/

### --- BODYPARTS

cd "$STAGE_SCRIPTDIR"
cd "$STAGE_WORK_ROOT"

mkdir -p makehuman-bodyparts/debroot/usr/share/makehuman/data

cd $WORKROOT/makehuman-src/data

mv eye* $WORKROOT/makehuman-bodyparts/debroot/usr/share/makehuman/data/
mv genitals $WORKROOT/makehuman-bodyparts/debroot/usr/share/makehuman/data/
mv teeth $WORKROOT/makehuman-bodyparts/debroot/usr/share/makehuman/data/
mv tongue $WORKROOT/makehuman-bodyparts/debroot/usr/share/makehuman/data/

### --- EXPRESSIONS

cd "$STAGE_SCRIPTDIR"
cd "$STAGE_WORK_ROOT"

mkdir -p makehuman-expressions/debroot/usr/share/makehuman/data

cd $WORKROOT/makehuman-src/data

mv expressions $WORKROOT/makehuman-expressions/debroot/usr/share/makehuman/data/

### --- RIGS

cd "$STAGE_SCRIPTDIR"
cd "$STAGE_WORK_ROOT"

mkdir -p makehuman-rigs/debroot/usr/share/makehuman/data

cd $WORKROOT/makehuman-src/data

mv rigs $WORKROOT/makehuman-rigs/debroot/usr/share/makehuman/data/

### --- SKINS

cd "$STAGE_SCRIPTDIR"
cd "$STAGE_WORK_ROOT"

mkdir -p makehuman-skins/debroot/usr/share/makehuman/data

cd $WORKROOT/makehuman-src/data

mv skins $WORKROOT/makehuman-skins/debroot/usr/share/makehuman/data/

### --- HAIR

cd "$STAGE_SCRIPTDIR"
cd "$STAGE_WORK_ROOT"

mkdir -p makehuman-hair/debroot/usr/share/makehuman/data

cd $WORKROOT/makehuman-src/data

mv hair $WORKROOT/makehuman-hair/debroot/usr/share/makehuman/data/

### --- I18N

cd "$STAGE_SCRIPTDIR"
cd "$STAGE_WORK_ROOT"

mkdir -p makehuman-i18n/debroot/usr/share/makehuman/data

cd $WORKROOT/makehuman-src/data

mv languages $WORKROOT/makehuman-i18n/debroot/usr/share/makehuman/data/

### --- PLUGINS

cd "$STAGE_SCRIPTDIR"
cd "$STAGE_WORK_ROOT"

mkdir -p makehuman-plugins/debroot/usr/share/makehuman

cd $WORKROOT/makehuman-src

mv plugins $WORKROOT/makehuman-plugins/debroot/usr/share/makehuman

### --- DATA

cd "$STAGE_SCRIPTDIR"
cd "$STAGE_WORK_ROOT"

mkdir -p makehuman-data/debroot/usr/share/makehuman

cd $WORKROOT/makehuman-src

mv data $WORKROOT/makehuman-data/debroot/usr/share/makehuman

### --- CORE

cd "$STAGE_SCRIPTDIR"
cd "$STAGE_WORK_ROOT"

mkdir -p makehuman-release/debroot/usr/share/applications
mkdir -p makehuman-release/debroot/usr/bin

cd $WORKROOT

mv makehuman-src $WORKROOT/makehuman-release/debroot/usr/share/makehuman

rm -f $WORKROOT/makehuman-release/debroot/usr/share/makehuman/license.txt
rm -f $WORKROOT/makehuman-release/debroot/usr/share/makehuman/makehuman
  
cat "$STAGE_SCRIPTDIR/unbundled/MakeHuman.desktop" | sed -e "s/VERSION/${STAGE_RELEASE_TAG}/g" > "makehuman-release/debroot/usr/share/applications/MakeHuman.desktop"
chown 0:0 "makehuman-release/debroot/usr/share/applications/MakeHuman.desktop"
chmod 644 "makehuman-release/debroot/usr/share/applications/MakeHuman.desktop"

cp "$STAGE_SCRIPTDIR/unbundled/makehuman" "makehuman-release/debroot/usr/bin/makehuman"
chown 0:0 "makehuman-release/debroot/usr/bin/makehuman"
chmod 755 "makehuman-release/debroot/usr/bin/makehuman"

### -- ACTUAL BUILDING

for f in "${debs[@]}"
do
  cd $WORKROOT/$f
  SIZE=`du -cks debroot | cut -f 1 | head -1 | xargs echo -n`
  cat "$STAGE_SCRIPTDIR/control/$f" | sed -e "s/SIZE/${SIZE}/g" | sed -e "s/VERSION/${TOTVER}/g" > "debroot/DEBIAN/control"
  cd $WORKROOT/$f/build
  $DPKG "$f-${STAGE_RELEASE_TAG}-${SUBVER}.deb"
  lintian --info "$f-${STAGE_RELEASE_TAG}-${SUBVER}.deb" &> lintian.txt
  mv "$f-${STAGE_RELEASE_TAG}-${SUBVER}.deb" $WORKROOT/
done

cd $OLDCWD


